﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;


namespace FileCryptApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string sourceFileName; // name of the file to be decrypted
            string destFileName; // name of the encrypted file
            string pwd; // password string
            bool srcFileExists; 

            // getting the source file name for encryption / decrypion
            do
            {
                Console.WriteLine("Enter name of file to encrypt or decrypt in current directory... ");
                Console.Write("Files with extencion *.aes will be decrypted, other then *.aes extensions - encrypted");
                sourceFileName = Console.ReadLine();
                srcFileExists = File.Exists(sourceFileName);
            }
            while (!srcFileExists);

            // getting the password for encryptrion / decrypion
            do
            {
                Console.Write("Enter the password for encryption / decrypion (at least 6 English small or capital letters, digits)... ");
                pwd = Console.ReadLine();
            }
            while (!Regex.IsMatch(pwd, @"^[a-zA-Z0-9]{6,}$"));

            // defining the task: decryption or eccryption IAW extension
            if (sourceFileName.EndsWith(".aes"))
 
            {   // decrpytion branch:
                try
                {
                    destFileName = sourceFileName.Remove(sourceFileName.Length - 4);
                    FCrypt.Decrypt(sourceFileName, pwd, destFileName);
                    Console.WriteLine("Derypted. Destination file name: {0}", destFileName);
                }
                catch {Console.Write("Wrong pwd"); }
                
            }

            else
            {   // encryption branch:
                destFileName = sourceFileName + ".aes";
                FCrypt.Encrypt(sourceFileName, pwd, destFileName);
                Console.WriteLine("Encrypted. Destination file name: {0}", destFileName);
            }

            Console.ReadLine();
        }
    }
}
